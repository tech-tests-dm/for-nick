package com.akordirect.ig.core.utils.extentions

import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import com.akordirect.ig.R
import com.skydoves.landscapist.glide.GlideImage


@Composable
fun LoadNetworkImage(
    image: String,
    modifier: Modifier,
    contentScale: ContentScale = ContentScale.FillBounds,
    alignment: Alignment = Alignment.TopStart,
) {
    GlideImage(
        imageModel = image,
        contentScale = contentScale,
        placeHolder = ImageBitmap.imageResource(R.drawable.ic_app_placeholder),
        alignment = alignment,
        modifier = modifier
    )
}
