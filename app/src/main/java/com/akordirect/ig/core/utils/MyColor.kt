package com.akordirect.ig.core.utils

import androidx.compose.ui.graphics.Color

val gradientColor1 = Color(0xFFEEF9FF)
val gradientColor2 = Color(0xFFF3E4E7)
val gradientColor3 = Color(0xFFF5F1FF)
val gradientColor4 = Color(0xFFEDF6FD)
val gradientColor5 = Color(0xFFF7EDE4)
