package com.akordirect.ig.core.utils.extentions

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp


@Composable
fun Int.SpacerHeight() {
    Spacer(modifier = Modifier.height(this.dp))
}

@Composable
fun Int.SpacerWidth() {
    Spacer(modifier = Modifier.width(this.dp))
}

@Composable
fun Int.radius(): RoundedCornerShape = RoundedCornerShape(this.dp)



