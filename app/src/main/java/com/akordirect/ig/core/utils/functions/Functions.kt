package com.akordirect.ig.core.utils.functions


fun isUpdateUI(previous: Int, marketValue: Int, change: (Int) -> Unit): Boolean {
    val isShow: Boolean =
        when (previous) {
            -1 -> true
            marketValue -> false
            else -> true
        }
    change.invoke(marketValue)
    return isShow
}
