package com.akordirect.ig.core.utils.extentions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import java.io.IOException

fun Context.assetsToBitmap(fileName: String): Bitmap? =
    try {
        with(assets.open(fileName)) {
            BitmapFactory.decodeStream(this)
        }
    } catch (e: IOException) {
        null
    }


val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "on_boarding_pref")
