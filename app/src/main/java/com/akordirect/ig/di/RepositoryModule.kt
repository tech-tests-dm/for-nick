package com.akordirect.ig.di


import android.content.Context
import com.akordirect.ig.data.local.DatabaseDm
import com.akordirect.ig.data.remote.RetrofitApiService
import com.akordirect.ig.data.repositories.*
import com.akordirect.ig.domain.repositories.MarketRepository
import com.akordirect.ig.domain.repositories.PreferencesRepository
import com.akordirect.ig.domain.repositories.PricesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideDataStoreRepository(@ApplicationContext context: Context): PreferencesRepository =
        PreferencesRepositoryImpl(context = context)


    @Singleton
    @Provides
    fun provideRecipeRepository(db: DatabaseDm): PricesRepository = PricesRepositoryImpl(db)

    @Singleton
    @Provides
    fun provideMarketRepository(retrofitService: RetrofitApiService, db: DatabaseDm): MarketRepository =
        MarketRepositoryImpl(service = retrofitService, db = db)

}