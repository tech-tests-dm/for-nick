package com.akordirect.ig.di

import android.content.Context
import androidx.room.Room
import com.akordirect.ig.data.local.DatabaseDm
import com.akordirect.ig.data.local.dao.MarketDao
import com.akordirect.ig.core.utils.constants.IG_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {


    @Provides
    @Singleton
    fun provideMarketDao(databaseDm: DatabaseDm): MarketDao = databaseDm.marketDao()


    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): DatabaseDm =
        Room.databaseBuilder(context, DatabaseDm::class.java, IG_DATABASE)
            .fallbackToDestructiveMigration()
            .build()


}