package com.akordirect.ig.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.akordirect.ig.domain.model.Market


@Entity(tableName = "markets")
data class MarketEntity(

    @field:PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var name: String = "",
    var image: String = ""
){

    fun toMarket(): Market =
        Market(name = name, image = image)
}
