package com.akordirect.ig.data.local.dao

import androidx.room.*
import com.akordirect.ig.data.local.entity.MarketEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface MarketDao {

    @Query("SELECT * FROM markets")
     fun allMarketEntities(): Flow<List<MarketEntity>>

    @Query("SELECT * from markets where id =:id")
    suspend fun getMarketEntityById(id: String): MarketEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addMarket(market: MarketEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(market: MarketEntity)

    @Delete
    suspend fun deleteMarket(market: MarketEntity)

    @Query("DELETE FROM markets")
    suspend fun deleteAll()

}