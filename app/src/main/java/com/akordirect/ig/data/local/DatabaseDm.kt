package com.akordirect.ig.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.akordirect.ig.data.local.dao.MarketDao
import com.akordirect.ig.data.local.entity.MarketEntity

@Database(
    entities = arrayOf(
        MarketEntity::class,
    ),
    version = 7
)
@TypeConverters()
abstract class DatabaseDm : RoomDatabase() {

    abstract fun marketDao(): MarketDao

    suspend fun clearData() {
        marketDao().deleteAll()
    }

}