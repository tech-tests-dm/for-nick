package com.akordirect.ig.data.local

const val ic_down="ic_down.png"
const val ic_up="ic_up.png"

const val ic_image_tesla="https://www.pngmart.com/files/10/Tesla-Logo-PNG-Clipart.png"
const val ic_image_gold="https://thumbor.forbes.com/thumbor/fit-in/900x510/https://www.forbes.com/advisor/de/wp-content/uploads/2022/05/gold-ingots-2021-08-29-05-54-54-utc-Cropped.jpg"
const val ic_image_apple="https://pbs.twimg.com/profile_images/1174747027986452480/cSlw47L-_400x400.png"
const val ic_image_oil="https://img.etimg.com/thumb/msid-88854302,width-300,imgsize-20918,,resizemode-4,quality-100/oil.jpg"
const val ic_image_other="https://image.freepik.com/free-photo/displeased-bearded-guy-smirks-face-uses-modern-cellphone-has-sad-expression-wears-transparent-glasses-jumper_273609-34133.jpg"

const val ic_intro1="ic_intro1.jpg"
const val ic_intro3="edu_ic_intro3.png"
const val ic_intro4="edu_ic_intro4.png"