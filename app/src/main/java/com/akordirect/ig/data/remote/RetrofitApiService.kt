package com.akordirect.ig.data.remote

import com.akordirect.ig.data.remote.dto.MarketDto
import retrofit2.Response
import retrofit2.http.GET


interface RetrofitApiService {


    @GET("ig/markets")
    suspend fun markets(): Response<List<MarketDto>>

}