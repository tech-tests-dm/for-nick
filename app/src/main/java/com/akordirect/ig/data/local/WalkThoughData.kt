package com.akordirect.ig.data.local

import android.os.Parcel
import android.os.Parcelable


data class EduBaseModel(
    var image: String? = null,
    var title: String? = null,
    var description: String? = null,
    var rating: String? = null,
    var progress:Float? =null,
    var marks: String? = null,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Float::class.java.classLoader) as? Float,
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(rating)
        parcel.writeValue(progress)
        parcel.writeString(marks)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<EduBaseModel> {
        override fun createFromParcel(parcel: Parcel): EduBaseModel = EduBaseModel(parcel)
        override fun newArray(size: Int): Array<EduBaseModel?> = arrayOfNulls(size)
    }
}

val walkThoughData = listOf(
    EduBaseModel(
        image = ic_intro1,
        title = "Track price\n changes easily ",
        description = "SIMPLE WAY TO TRACK PRICE CHANGES, YET EFFECTIVE INSIGHTS INTO YOUR MARKET."
    ),
    EduBaseModel(
        image = ic_intro3,
        title = "Who Should \n Track Prices",
        description = "Any and all businesses need price tracking. It’s clear that if you don’t track price changes, almost no business can operate effectively and reach its growth goals."
    ),
    EduBaseModel(
        image = ic_intro4,
        title = "How to Benefit from \n Price Tracking",
        description = "To effectively track price changes you will need assistance in the form of software and service providers"
    ),
)
