package com.akordirect.ig.data.remote.dto

import com.akordirect.ig.data.local.entity.MarketEntity
import com.akordirect.ig.domain.model.Market
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MarketDto(
    val id: Long = 0,
    @Json(name = "name") var name: String = "",
    @Json(name = "image") val image: String = ""
) {
    fun toMarket() = Market(name = name, image = image)

    fun toMarketEntity(): MarketEntity =
        MarketEntity(name = name, image = image)
}


fun List<MarketEntity>.toListMarketsDto() =
    this.map {
        MarketDto(
            name = it.name,
            image = it.image
        )
    }

fun List<MarketDto>?.toListMarkets() =
    this?.map {
        Market(name = it.name, image = it.image)
    } ?: emptyList()
