package com.akordirect.ig.data.repositories

import com.akordirect.ig.data.local.DatabaseDm
import com.akordirect.ig.data.remote.dto.MarketDto
import com.akordirect.ig.domain.model.Direction
import com.akordirect.ig.domain.model.Price
import com.akordirect.ig.domain.repositories.PricesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlin.random.Random

class PricesRepositoryImpl(
    private val db: DatabaseDm
    ) : PricesRepository {

    private var marketName = ""

    override fun startFlowOfPrices(name: String): Flow<Price> {
        marketName = name
        return flow() {
            var previous = -1

            while (true) {
                val random = Random.nextInt(50, 100)

                val direction: Direction = when {
                    previous == -1 -> Direction.UNKNOWN
                    previous > random -> Direction.DOWN
                    previous < random -> Direction.UP
                    else -> Direction.UNKNOWN
                }
                previous = random

                val price = Price(marketName, random, direction)
                emit(price)
                delay(300L)
            }
        }.flowOn(Dispatchers.Default)
    }

    override fun changeMarket(name: String) {
        marketName = name
    }

    override fun saveInCache(market: MarketDto, scope: CoroutineScope) {
        scope.launch {
            db.marketDao().addMarket(market.toMarketEntity())
        }
    }

    override fun getAllPrices(scope: CoroutineScope) {
        scope.launch {
//          val tt: Flow<List<MarketEntity>> =  db.marketDao().allMarket()
//            tt.collect {
//                it.forEach{ market ->
//                    Log.d("gg", "dm:: from DB: ${market.name} ${market.value}")
//                }

//            }
        }
    }

    override fun removeAllMarkets(scope: CoroutineScope) {
        scope.launch {
            db.marketDao().deleteAll()
        }
    }


}