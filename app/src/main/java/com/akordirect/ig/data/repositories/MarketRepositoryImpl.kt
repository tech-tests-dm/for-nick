package com.akordirect.ig.data.repositories

import com.akordirect.ig.data.local.DatabaseDm
import com.akordirect.ig.data.remote.RetrofitApiService
import com.akordirect.ig.data.remote.dto.MarketDto
import com.akordirect.ig.data.remote.dto.toListMarketsDto
import com.akordirect.ig.domain.repositories.MarketRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import retrofit2.Response

class MarketRepositoryImpl(
    private val service: RetrofitApiService,
    private val db: DatabaseDm,
): MarketRepository {

    override suspend fun getListMarkets(): Response<List<MarketDto>> = service.markets()


     override  fun flowOfMarkets(): Flow<List<MarketDto>> =
         db.marketDao().allMarketEntities().map {
             it.toListMarketsDto()
         }


}