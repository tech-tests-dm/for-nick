package com.akordirect.ig.presentation.navigation


import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.akordirect.ig.presentation.screens.MainScreen
import com.akordirect.ig.presentation.screens.SplashScreen
import com.akordirect.ig.presentation.screens.WelcomeScreen
import com.google.accompanist.pager.ExperimentalPagerApi


@OptIn(
    ExperimentalFoundationApi::class, ExperimentalMaterialApi::class,
    ExperimentalPagerApi::class
)
@Composable
fun MarketsApp(
    navController: NavHostController
) {
    NavHost(
        navController,
        startDestination = MyScreens.Splash.name
    ) {


        composable(route = MyScreens.Splash.name) {
            SplashScreen { screenName ->
                navController.navigate(screenName) {
                    popUpTo(0)
                }
            }
        }

        this.composable(route = MyScreens.WalkThrough.name) {
            WelcomeScreen(navController)
        }

        this.composable(route = MyScreens.Main.name) {
            MainScreen()
        }

    }
}
