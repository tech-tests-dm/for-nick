package com.akordirect.ig.presentation.vm

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.akordirect.ig.domain.repositories.PreferencesRepository
import com.akordirect.ig.presentation.navigation.MyScreens
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SplashViewModel @Inject constructor(
    private val repository: PreferencesRepository
) : ViewModel() {


    private val _startDestination: MutableState<String> = mutableStateOf(MyScreens.WalkThrough.name)
    val startDestination: State<String> = _startDestination

    init {
        viewModelScope.launch {

            repository.readOnBoardingState().collect { completed ->
                if (completed) {
                    _startDestination.value = MyScreens.Main.name
                } else {
                    _startDestination.value = MyScreens.WalkThrough.name
                }
            }
        }
    }

}