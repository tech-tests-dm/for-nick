package com.akordirect.ig.presentation.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.akordirect.ig.core.utils.extentions.assetsToBitmap
import com.akordirect.ig.presentation.vm.SplashViewModel
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.delay

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@DelicateCoroutinesApi
@SuppressLint("CoroutineCreationDuringComposition")

@Composable
fun SplashScreen(
    splashViewModel: SplashViewModel = hiltViewModel(),
    goToScreen: (String) -> Unit
) {

    val screen: String by splashViewModel.startDestination

    SplashScreenContent()

    LaunchedEffect(Unit) {
        delay(3000)
        goToScreen(screen)
    }
}


@Composable
fun SplashScreenContent() {

    val context = LocalContext.current

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            bitmap = context.assetsToBitmap("app_icon.png")!!.asImageBitmap(),
            contentDescription = "",
            modifier = Modifier.height(500.dp)
        )
    }

}

@Preview(showSystemUi = true)
@Composable
fun SplashScreenContentPriview() {
    SplashScreenContent()
}