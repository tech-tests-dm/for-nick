package com.akordirect.ig.presentation.screens

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.akordirect.ig.core.utils.extentions.*
import com.akordirect.ig.data.local.walkThoughData
import com.akordirect.ig.presentation.navigation.MyScreens
import com.akordirect.ig.presentation.vm.WelcomeViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState


@ExperimentalPagerApi
@Composable
fun WelcomeScreen(
    navController: NavHostController,
    welcomeViewModel: WelcomeViewModel = hiltViewModel()
) {

    val pagerState: PagerState = run { rememberPagerState(pageCount = walkThoughData.size) }


    Box(
        modifier = Modifier.fillMaxSize()

    ) {

        PagerScreen(pagerState)

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
                .padding(bottom = 35.dp, start = 16.dp, end = 16.dp),
            content = {
                Row(content = {
                    walkThoughData.forEachIndexed { index, _ ->
                        WalkDots(
                            selected = index == pagerState.currentPage,
                            MaterialTheme.colors.primary
                        )
                    }
                })

                16.SpacerHeight()

                AnimatedVisibility(pagerState.currentPage == walkThoughData.size.minus(1)) {
                    Button(
                        onClick = {
                            welcomeViewModel.saveOnBoardingState(isPageShown = true)
                            navController.popBackStack()
                            navController.navigate(MyScreens.Main.name)
                        },
                        shape = 40.radius(),
                        content = {
                            Text(
                                "Get Started",
                                style = boldTextStyle(color = Color.White),
                                modifier = Modifier.padding(4.dp)
                            )
                        }
                    )
                }

            },
        )
    }

}


@ExperimentalPagerApi
@Composable
fun PagerScreen(pagerState: PagerState) {

    val context = LocalContext.current

    HorizontalPager(
        state = pagerState,
        modifier = Modifier.fillMaxSize(),
        content = { page ->
            Column(
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    bitmap = context.assetsToBitmap(walkThoughData[page].image!!)!!
                        .asImageBitmap(),
                    contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .height(350.dp),
                    contentDescription = ""
                )

                20.SpacerHeight()

                Text(
                    text = walkThoughData[page].title!!,
                    style = boldTextStyle(fontSize = 26.sp, color = Color.Blue)
                )
                Text(
                    text = walkThoughData[page].description!!,
                    style = secondaryTextStyle(
                        textAlign = TextAlign.Center,
                        fontSize = 16.sp
                    ),
                    lineHeight = 22.sp,
                    modifier = Modifier.padding(
                        start = 24.dp,
                        bottom = 16.dp,
                        end = 24.dp,
                        top = 16.dp
                    )
                )
                8.SpacerHeight()
            }
        })
}


@ExperimentalPagerApi
@Preview
@Composable
fun PagerScreen_Preview() {
    val pagerState: PagerState = run { rememberPagerState(pageCount = walkThoughData.size) }
    PagerScreen(pagerState = pagerState)
}


@Composable
fun WalkDots(selected: Boolean, color: Color) =
    Box(
        modifier = Modifier
            .padding(4.dp)
            .clip(CircleShape)
            .background(if (selected) color else Color.Gray)
            .height(10.dp)
            .width(10.dp)
    )