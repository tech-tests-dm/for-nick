package com.akordirect.ig.presentation.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.akordirect.ig.domain.repositories.PreferencesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WelcomeViewModel @Inject constructor(
    private val repository: PreferencesRepository
) : ViewModel() {


    fun saveOnBoardingState(isPageShown: Boolean) =
        this.viewModelScope.launch(Dispatchers.IO) {
            repository.saveOnBoardingState(completed = isPageShown)
        }

}