@file:Suppress("OPT_IN_IS_NOT_ENABLED")

package com.akordirect.ig.presentation.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FilterList
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.akordirect.ig.R
import com.akordirect.ig.core.utils.*
import com.akordirect.ig.core.utils.extentions.*
import com.akordirect.ig.domain.model.Market
import com.akordirect.ig.domain.model.Price
import com.akordirect.ig.presentation.components.ErrorUI
import com.akordirect.ig.presentation.components.LoadingUI
import com.akordirect.ig.presentation.vm.MainViewModel


@OptIn(ExperimentalLifecycleComposeApi::class)
@Composable
fun MainScreen(
    vm: MainViewModel = hiltViewModel()
) {

    val price by vm.newPrice.collectAsStateWithLifecycle()
    val listOfMarketName by vm.listOfMarketName





    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(id = R.string.market_prices)) },
                actions = {
                    IconButton(
                        onClick = {
                            vm.printAllPrices()
                        },
                        content = { Icon(Icons.Filled.FilterList, "Reload") }
                    )
                }
            )
        },

        ) { padding ->
        Column(
            modifier = Modifier
                .padding(padding)
                .fillMaxHeight()
                .background(
                    brush = Brush.verticalGradient(
                        colors = listOf(
                            gradientColor1,
                            gradientColor2,
                            gradientColor3,
                            gradientColor4,
                            gradientColor5,
                        )
                    )
                )
        ) {
            when {
                vm.isLoading -> LoadingUI()
                vm.isError -> ErrorUI()
                else -> {
                    PriceHeaderWidget(price)
                    LazyColumn {
                        items(listOfMarketName) { marketName ->
                            MarketItem(item = marketName) {
                                vm.changeMarket(it)
                            }
                        }
                    }
                }
            }
        }
    }
}


@Composable
fun PriceHeaderWidget(price: Price) {
    val context = LocalContext.current
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp, end = 20.dp),
        content = {

            LoadNetworkImage(
                image = price.image,
                modifier = Modifier
                    .clip(CircleShape)
                    .size(80.dp),
                contentScale = ContentScale.Crop
            )
            10.SpacerHeight()

            price.arrow?.let { imageStr ->
                Image(
                    bitmap = context.assetsToBitmap(imageStr)!!.asImageBitmap(),
                    modifier = Modifier
                        .width(20.dp)
                        .height(20.dp),
                    contentDescription = "", contentScale = ContentScale.Crop
                )
            } ?: run {
                20.SpacerHeight()
            }

            10.SpacerHeight()
            Text(price.marketName, style = boldTextStyle(), color = Color.Blue)
            Text("${price.value}", style = boldTextStyle(fontSize = 18.sp, color = Color.Black))
        })
}


@Composable
fun MarketItem(item: Market, go: (String) -> Unit) {
    Row(
        modifier = Modifier
            .padding(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)
            .clickable { go.invoke(item.name) }
    ) {

        Box(
            modifier = Modifier
                .clip(40.radius())
                .background(MaterialTheme.colors.surface)
                .fillMaxWidth()
                .padding(start = 4.dp, top = 4.dp, bottom = 4.dp, end = 16.dp),
            content = {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        content = {
                            LoadNetworkImage(
                                image = item.image, modifier = Modifier
                                    .clip(CircleShape)
                                    .size(40.dp), contentScale = ContentScale.Crop
                            )
                            10.SpacerWidth()
                            Text(item.name, style = boldTextStyle(), color = Color.DarkGray)
                        },
                    )
                }
            },
        )
    }
}