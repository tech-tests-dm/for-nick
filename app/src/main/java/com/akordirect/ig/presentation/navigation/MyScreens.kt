package com.akordirect.ig.presentation.navigation


enum class MyScreens() {
   Splash, WalkThrough, Main
}