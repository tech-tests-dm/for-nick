package com.akordirect.ig.presentation.vm

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.akordirect.ig.core.utils.functions.isUpdateUI
import com.akordirect.ig.data.remote.dto.MarketDto
import com.akordirect.ig.data.remote.dto.toListMarkets
import com.akordirect.ig.domain.model.Market
import com.akordirect.ig.domain.model.Price
import com.akordirect.ig.domain.repositories.MarketRepository
import com.akordirect.ig.domain.repositories.PricesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val priceRepository: PricesRepository,
    private val marketRepository: MarketRepository,
) :
    ViewModel() {

    private var _newPrice = MutableStateFlow(Price())
    var newPrice: StateFlow<Price> = _newPrice

    var listOfMarketName = mutableStateOf(listOf<Market>())

    val errorHandler = CoroutineExceptionHandler { _, throwable ->
        isLoading = false
        isError = true
    }

    var isLoading by mutableStateOf(false)
    var isError by mutableStateOf(false)

    var privious = -1

    init {
        startFlowOfPrices("Apple")
        marketsFromServer()
    }

    fun changeMarket(name: String){
        priceRepository.changeMarket(name)
    }

    fun printAllPrices(){
        priceRepository.getAllPrices(viewModelScope)
    }


    private fun startFlowOfPrices(name: String) {
        newPrice = priceRepository.startFlowOfPrices(name)
          .filter { price ->
                isUpdateUI(previous = privious, marketValue = price.value) { privious = it }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000),
                initialValue = Price()
            )
    }


    private fun marketsFromServer() {
        viewModelScope.launch(errorHandler) {
            isLoading = true
            val resp: Response<List<MarketDto>> = marketRepository.getListMarkets()

            if (resp.isSuccessful) {
                val listResult = resp.body()

                listResult?.forEach {
                    priceRepository.saveInCache(it, viewModelScope)
                } ?: run {
                    priceRepository.removeAllMarkets(viewModelScope)
                }

                listOfMarketName.value = listResult.toListMarkets()

            } else {
                isLoading = false
                isError = true
            }
            isLoading = false
        }

    }

}


