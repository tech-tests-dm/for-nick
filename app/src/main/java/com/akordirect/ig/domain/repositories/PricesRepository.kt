package com.akordirect.ig.domain.repositories

import com.akordirect.ig.data.remote.dto.MarketDto
import com.akordirect.ig.domain.model.Price
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

interface PricesRepository {

    fun startFlowOfPrices(name: String): Flow<Price>
    fun changeMarket(name: String)

    fun saveInCache(market: MarketDto, scope: CoroutineScope)
    fun getAllPrices(scope: CoroutineScope)

    fun removeAllMarkets(scope: CoroutineScope)

}