package com.akordirect.ig.domain.repositories

import kotlinx.coroutines.flow.Flow


interface PreferencesRepository {
    suspend fun saveOnBoardingState(completed: Boolean)
    fun readOnBoardingState(): Flow<Boolean>
}