package com.akordirect.ig.domain.model

enum class Direction {
    UP, DOWN, UNKNOWN
}