package com.akordirect.ig.domain.repositories

import com.akordirect.ig.data.remote.dto.MarketDto
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface MarketRepository {

    suspend fun getListMarkets(): Response<List<MarketDto>>

     fun flowOfMarkets(): Flow<List<MarketDto>>
}