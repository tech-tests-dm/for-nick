package com.akordirect.ig.domain.model

import com.akordirect.ig.data.local.*


data class Price(
    val marketName: String = "",
    val value: Int = 0,
    val direction: Direction = Direction.UNKNOWN
) {

    val arrow: String? =
        when (direction) {
            Direction.UP -> ic_up
            Direction.DOWN -> ic_down
            else -> null
        }

    val image: String =
        when (marketName) {
            "Gold" -> ic_image_gold
            "Tesla" -> ic_image_tesla
            "Apple" -> ic_image_apple
            "Oil" -> ic_image_oil
            else -> ic_image_other
        }

}