package com.akordirect.ig.vm

import com.akordirect.ig.core.utils.functions.isUpdateUI
import com.google.common.truth.Truth
import org.junit.Test

class MainViewModelTest {


    @Test
    fun `is Update UI test -1 and 50`() {
        val previous = -1
        val marketValue = 50

        val isShow = isUpdateUI(previous = previous, marketValue = marketValue) { }
        println("dm:: previous = $previous,  marketValue = $marketValue. Result show: $isShow")
        Truth.assertThat(isShow).isTrue()
    }

    @Test
    fun `is Update UI test (5 and 50)`() {
        val previous = 5
        val marketValue = 50

        val isShow = isUpdateUI(previous = previous, marketValue = marketValue) { }
        println("dm:: previous = $previous,  marketValue = $marketValue. Result show: $isShow")
        Truth.assertThat(isShow).isTrue()
    }

    @Test
    fun `is Update UI test (40 and 10)`() {
        val previous = 40
        val marketValue = 10

        val isShow = isUpdateUI(previous = previous, marketValue = marketValue) { }
        println("dm:: previous = $previous,  marketValue = $marketValue. Result show: $isShow")
        Truth.assertThat(isShow).isTrue()
    }

    @Test
    fun `is Update UI test (5 and 5)`() {
        val previous = 5
        val marketValue = 5

        val isShow = isUpdateUI(previous = previous, marketValue = marketValue) { }
        println("dm:: previous = $previous,  marketValue = $marketValue. Result show: $isShow")
        Truth.assertThat(isShow).isFalse()
    }

    @Test
    fun `is market value changed`() {
        var previous = 20
        val marketValue = 5

        Truth.assertThat(previous).isEqualTo(20)
        println("dm:: previous = $previous,  marketValue = $marketValue.")

        isUpdateUI(previous = previous, marketValue = marketValue) {
            previous = it
        }
        println("dm:: previous = $previous,  marketValue = $marketValue")
        Truth.assertThat(previous).isEqualTo(5)
    }


}