package com.akordirect.ig.data.repositories

import app.cash.turbine.test
import com.akordirect.ig.domain.model.Direction
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class PricesRepositoryImplTest {

    lateinit var rep: PricesRepositoryImpl

    @Before
    fun setUp() {
        rep = PricesRepositoryImpl()
    }

    @Test
    fun getFlowOfPrices() = runBlocking {
        rep.startFlowOfPrices("Audi").test {
            for (i in 0 until 10) {
                val emit = awaitItem()
                Truth.assertThat(emit.value).isIn(50..100)
                println("$i)  - ${emit.value}")
            }
        }
    }

    @Test
    fun `getFlowOfPrices for name`() = runBlocking {
        rep.startFlowOfPrices("Audi").test {
            for (i in 0 until 5) {
                val emit = awaitItem()
                Truth.assertThat(emit.marketName).isEqualTo("Audi")
                println("$i)  - ${emit.marketName}")
            }
        }
    }

    @Test
    fun `getFlowOfPrices for direction`() = runBlocking {
        rep.startFlowOfPrices("Audi").test {

            val emit = awaitItem()
            Truth.assertThat(emit.direction).isEqualTo(Direction.UNKNOWN)
            println("dm:: Direction: ${Direction.UNKNOWN.name}")

        }
    }

    @Test
    fun `getFlowOfPrices for direction 2`() = runBlocking {
        rep.startFlowOfPrices("Audi").test {
            var previous = -1
            for (i in 0 until 10) {
                val emit = awaitItem()

                when {
                    previous == -1 -> {
                        println("dm::  $i) Direction: ${emit} ($previous == -1)")
                        Truth.assertThat(emit.direction).isEqualTo(Direction.UNKNOWN)
                    }
                    previous > emit.value -> {
                        println("dm::  $i) Direction: ${emit} ($previous > ${emit.value})")
                        Truth.assertThat(emit.direction).isEqualTo(Direction.DOWN)
                    }
                    previous < emit.value -> {
                        println("dm::  $i) Direction: ${emit} ($previous < ${emit.value})")
                        Truth.assertThat(emit.direction).isEqualTo(Direction.UP)
                    }
                    else -> {
                        println("dm::  $i) Direction: ${emit} ($previous = ${emit.value})")
                        Truth.assertThat(emit.direction).isEqualTo(Direction.UNKNOWN)

                    }
                }
                previous = emit.value
            }
        }
    }
}